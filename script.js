"use strict";
function createNewUser() {
    const firstName = prompt('Enter your  first name');
    const lastName = prompt('Enter your last name');
    const birthday = prompt('Enter your birthday (dd.mm.yyyy)');
    const newUser = {
        firstName,
        lastName,
        getAge () {
            const birthdaySplit = birthday.split('.');
            return ((new Date().getTime() - new Date(`${birthdaySplit[2]}-${birthdaySplit[1]}-${birthdaySplit[0]}`)) / (24 * 3600 * 365 * 1000));
        },
        getLogin () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getPassword () {
            const birthdaySplit = birthday.split('.');
            return (this.firstName[0].toUpperCase()+this.lastName.toLowerCase()+birthdaySplit[2]);
        },
    };
    Object.defineProperty(newUser, "firstName", {
        writable: false,
        configurable: true,
    });
    newUser.setUserName = function (firstName) {
        Object.defineProperty(this, "firstName", {
            value: firstName
        });
    };
    Object.defineProperty(newUser, "lastName", {
        writable: false,
        configurable: true,
    });
    newUser.setSecondName = function (lastName) {
        Object.defineProperty(this, "lastName", {
            value: lastName
        });
    };
    return newUser;
}
let newNewOb = createNewUser();
console.log(newNewOb.getLogin());
console.log(newNewOb.getAge());
console.log(newNewOb.getPassword());

